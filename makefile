VERSION = 0.0.1

build:
	docker build -t squarel:$(VERSION) ./
	docker tag squarel:$(VERSION) $(USER)/squarel:$(VERSION)

push:
	docker push $(USER)/squarel:$(VERSION)

clean:
	docker rmi squarel:$(VERSION)
