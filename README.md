# Squarel
Squarel is a MySql docker image created for building containers running internal mysql servers with docker using the [Alpine](https://hub.docker.com/_/alpine/) image as a base. The [dockerfile](./dockerfile) is written in a format to be base dockerfile for 
another project. It does not execute mysql on start but lays the foundation for you to have a quick start for one.

It uses latest Mysql(mariadb-10.1.26) package from alpine package repository.

## Binary

The dockerfile creates a binary file `bootsql` in `/bin/` which starts mysql with relation to provided parameters.

## Build
To build just run the following:

```go
make build
```

You can set version of build by setting `VERSION` environment variable to set custom version. It defaults to `0.0.1`.

```go
VERSION=0.1.0 make build
```

## Push
To push just run the following after ensuring docker credentials for 
docker repository is set:

```go
make push
```

## Run

To run the docker image built using the docker file:

- To run as a non-deamon:

```bash
docker run -it ${image_name} bootsql
```

- To run as a deamon:

```bash
fork=true docker run -it ${image_name} bootsql 
```

## Configuration

See configuration file for customization in [Conf](./conf/mysql.cnf)

## Parameters

The parameters below help define and indicate areas which can be set at lunch of 
mysql binary to provide user wanted values for the server.

- MySql Port (Default: `3306`)
- MySql User (Default: `root`)
- MySql Root Password 
	- Default: `123456`
	- Environment Variable: `MYSQL_ROOT_PASSWORD`
- MySql DATABASE (Default DB To BOOT)
	- Environment Variable: `MYSQL_DATABASE`
- MySql Username 
	- Environment Variable: `MYSQL_USER`
- MySql User Password 
	- Environment Variable: `MYSQL_USER_PASSWORD`
- RUN AS DAEMON (To execute other binary in image `ENTRYPOINT` or `CMD`)
	- Environment Variable: `fork=true`

## Data Files

The database when started would attempt to store all data files within `/data/db/mysql`, which 
allows you to customly mount the underline filesystem of the server wheere the container of this 
image would be executed to ensure files are stored within the host and not the container.

